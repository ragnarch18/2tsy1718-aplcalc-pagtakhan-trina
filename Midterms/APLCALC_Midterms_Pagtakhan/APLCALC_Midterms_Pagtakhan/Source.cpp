#include<iostream>

using namespace std;


class Rect
{
public:
	int xPos;
	int yPos;
	float xAnchor;
	float yAnchor;
	int width;
	int height;
	Rect(int p_x, int p_y, float p_xAnchor, float p_yAnchor, int p_width, int p_height);

	bool didIntersectRect(Rect p_rectToCheck);
};

class Circ
{
public:
	int xPos;
	int yPos;
	int radius;
	Circ(int p_x, int p_y, int p_radius);
	bool didIntersectCirc(Circ p_circToCheck);
};

//RECTANGLE
Rect::Rect(int p_x, int p_y, float p_xAnchor, float p_yAnchor, int p_width, int p_height)
{
	xPos = p_x;
	yPos = p_y;
	width = p_width;
	height = p_height;
	xAnchor = p_xAnchor;
	yAnchor = p_yAnchor;
}

bool Rect::didIntersectRect(Rect p_rectToCheck)
{
	bool l_result = false;

	bool l_xIntersect = false;
	bool l_yIntersect = false;

	//Anchor at (0,0)
	if (xAnchor == 0 && yAnchor == 0)
	{
		//check if intersects x-axis
		if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos + p_rectToCheck.width)
		{
			l_xIntersect = true;
		}
		else if (xPos + width >= p_rectToCheck.xPos
			&& xPos + width <= p_rectToCheck.xPos + p_rectToCheck.width)
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos + p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
		else if (yPos + height >= p_rectToCheck.yPos
			&& yPos + height <= p_rectToCheck.yPos + p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
	}
	//Anchor at (0.5, 0)
	if (xAnchor == 0.5 && yAnchor == 0)
	{
		//check if intersects x-axis
		if (xPos - (width / 2) >= p_rectToCheck.xPos - (p_rectToCheck.width / 2) && xPos - (width / 2) <= p_rectToCheck.xPos + (p_rectToCheck.width / 2))
		{
			l_xIntersect = true;
		}
		else if (xPos + (width / 2) >= p_rectToCheck.xPos - (p_rectToCheck.width / 2)
			&& xPos + (width / 2) <= p_rectToCheck.xPos + (p_rectToCheck.width / 2))
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos + p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
		else if (yPos + height >= p_rectToCheck.yPos
			&& yPos + height <= p_rectToCheck.yPos + p_rectToCheck.height)
		{
			l_yIntersect = true;
		}

	}
		//Anchor at (0.5, 0.5)
		if (xAnchor == 0.5 && yAnchor == 0.5)
		{
			//check if intersects x-axis
			if (xPos - (width / 2) >= p_rectToCheck.xPos - (p_rectToCheck.width / 2) && xPos - (width / 2) <= p_rectToCheck.xPos + (p_rectToCheck.width / 2))
			{
				l_xIntersect = true;
			}
			else if (xPos + (width / 2) >= p_rectToCheck.xPos - (p_rectToCheck.width / 2)
				&& xPos + (width / 2) <= p_rectToCheck.xPos + (p_rectToCheck.width / 2))
			{
				l_xIntersect = true;
			}

			//check if intersects y-axis
			if (yPos -(height/2) >= p_rectToCheck.yPos -(p_rectToCheck.height/2) && yPos - (height/2) <= p_rectToCheck.yPos + (p_rectToCheck.height/2))
			{
				l_yIntersect = true;
			}
			else if (yPos + (height/2) >= p_rectToCheck.yPos - (p_rectToCheck.height/2)
				&& yPos + (height/2) <= p_rectToCheck.yPos + (p_rectToCheck.height/2))
			{
				l_yIntersect = true;
			}
	}
		//Anchor at (0.5, 1)
		if (xAnchor == 0.5 && yAnchor == 1)
		{
			//check if intersects x-axis
			if (xPos - (width / 2) >= p_rectToCheck.xPos - (p_rectToCheck.width / 2) && xPos - (width / 2) <= p_rectToCheck.xPos + (p_rectToCheck.width / 2))
			{
				l_xIntersect = true;
			}
			else if (xPos + (width / 2) >= p_rectToCheck.xPos - (p_rectToCheck.width / 2)
				&& xPos + (width / 2) <= p_rectToCheck.xPos + (p_rectToCheck.width / 2))
			{
				l_xIntersect = true;
			}

			//check if intersects y-axis
			if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos - p_rectToCheck.height)
			{
				l_yIntersect = true;
			}
			else if (yPos - height >= p_rectToCheck.yPos
				&& yPos - height <= p_rectToCheck.yPos - p_rectToCheck.height)
			{
				l_yIntersect = true;
			}
		}
	//Anchor at (1, 0)
	if (xAnchor == 1 && yAnchor == 0)
	{
		//check if intersects x-axis
		if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos - p_rectToCheck.width)
		{
			l_xIntersect = true;
		}
		else if (xPos - width >= p_rectToCheck.xPos
			&& xPos - width <= p_rectToCheck.xPos - p_rectToCheck.width)
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos + p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
		else if (yPos + height >= p_rectToCheck.yPos
			&& yPos + height <= p_rectToCheck.yPos + p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
	}

	//Anchor at (1, 0.5)
	if (xAnchor == 1 && yAnchor == 0.5)
	{
		//check if intersects x-axis
		if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos - p_rectToCheck.width)
		{
			l_xIntersect = true;
		}
		else if (xPos - width >= p_rectToCheck.xPos
			&& xPos - width <= p_rectToCheck.xPos - p_rectToCheck.width)
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos - (height / 2) >= p_rectToCheck.yPos - (p_rectToCheck.height / 2) && yPos - (height / 2) <= p_rectToCheck.yPos + (p_rectToCheck.height / 2))
		{
			l_xIntersect = true;
		}
		else if (yPos + (height / 2) >= p_rectToCheck.yPos - (p_rectToCheck.height / 2)
			&& yPos + (height / 2) <= p_rectToCheck.yPos + (p_rectToCheck.height / 2))
		{
			l_xIntersect = true;
		}
	}

	//Anchor at (1, 1)
	if (xAnchor == 1 && yAnchor == 1)
	{
		//check if intersects x-axis
		if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos - p_rectToCheck.width)
		{
			l_xIntersect = true;
		}
		else if (xPos - width >= p_rectToCheck.xPos
			&& xPos - width <= p_rectToCheck.xPos - p_rectToCheck.width)
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos - p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
		else if (yPos - height >= p_rectToCheck.yPos
			&& yPos - height <= p_rectToCheck.yPos - p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
	}
	//Anchor at (0, 1)
	if (xAnchor == 0 && yAnchor == 1)
	{
		//check if intersects x-axis
		if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos + p_rectToCheck.width)
		{
			l_xIntersect = true;
		}
		else if (xPos + width >= p_rectToCheck.xPos
			&& xPos + width <= p_rectToCheck.xPos + p_rectToCheck.width)
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos - p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
		else if (yPos - height >= p_rectToCheck.yPos
			&& yPos - height <= p_rectToCheck.yPos - p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
		
	}
	//Anchor at (0, 0.5)
	if (xAnchor == 0 && yAnchor == 0.5)
	{
		//check if intersects x-axis
		if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos + p_rectToCheck.width)
		{
			l_xIntersect = true;
		}
		else if (xPos + width >= p_rectToCheck.xPos
			&& xPos + width <= p_rectToCheck.xPos + p_rectToCheck.width)
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos - (height / 2) >= p_rectToCheck.yPos - (p_rectToCheck.height / 2) && yPos - (height / 2) <= p_rectToCheck.yPos + (p_rectToCheck.height / 2))
		{
			l_xIntersect = true;
		}
		else if (yPos + (height / 2) >= p_rectToCheck.yPos - (p_rectToCheck.height / 2)
			&& yPos + (height / 2) <= p_rectToCheck.yPos + (p_rectToCheck.height / 2))
		{
			l_xIntersect = true;
		}
	}

	l_result = l_xIntersect && l_yIntersect;

	return l_result;
}

//CIRCLE

Circ::Circ(int p_x, int p_y, int p_radius)
{
	xPos = p_x;
	yPos = p_y;
	radius = p_radius;
}

bool Circ::didIntersectCirc(Circ p_circToCheck)
{
	bool l_result = false;
	int a;
	int b;
	int c;

	//value of the legs
	a = xPos - p_circToCheck.xPos;
	b = yPos - p_circToCheck.yPos;

	//pythagorean theorem
	c = sqrt((a*a) + (b*b));

	if (c <= (radius + p_circToCheck.radius))
	{
		l_result = true;
	}

	return l_result;
}

void circleResult()
{
	//values for Circle A
	int x_a;
	int y_a;
	int radius_a;

	//values for Circle B
	int x_b;
	int y_b;
	int radius_b;


	cout << "===========================" << endl;
	cout << "Input values for Circle A" << endl;
	cout << "===========================" << endl;
	cout << "x Position: ";
	cin >> x_a;
	cout << "y Position: ";
	cin >> y_a;
	cout << "Radius: ";
	cin >> radius_a;

	cout << "===========================" << endl;
	cout << "Input values for Circle B" << endl;
	cout << "===========================" << endl;
	cout << "x Position: ";
	cin >> x_b;
	cout << "y Position: ";
	cin >> y_b;
	cout << "Radius: ";
	cin >> radius_b;

	Circ circleA = Circ(x_a, y_a, radius_a);
	Circ circleB = Circ(x_b, y_b, radius_b);

	if (circleA.didIntersectCirc(circleB))
	{
		cout << "The circles are intersecting." << endl;
	}
	else
	{
		cout << "The circles are not intersecting." << endl;
	}
}

void rectResult()
{
	//Values for Rect A
	int x_a;
	int y_a;
	float xAnchor_a;
	float yAnchor_a;
	int width_a;
	int height_a;

	//Values for Rect B
	int x_b;
	int y_b;
	float xAnchor_b;
	float yAnchor_b;
	int width_b;
	int height_b;

	cout << "===========================" << endl;
	cout << "Input values for Rectangle A" << endl;
	cout << "===========================" << endl;
	cout << "x Position: ";
	cin >> x_a;
	cout << "y Position: ";
	cin >> y_a;
	cout << "x Anchor: ";
	cin >> xAnchor_a;
	cout << "y Anchor: ";
	cin >> yAnchor_a;
	cout << "Width: ";
	cin >> width_a;
	cout << "Height: ";
	cin >> height_a;

	cout << "===========================" << endl;
	cout << "Input values for Rectangle B" << endl;
	cout << "===========================" << endl;
	cout << "x Position: ";
	cin >> x_b;
	cout << "y Position: ";
	cin >> y_b;
	cout << "x Anchor: ";
	cin >> xAnchor_b;
	cout << "y Anchor: ";
	cin >> yAnchor_b;
	cout << "Width: ";
	cin >> width_b;
	cout << "Height: ";
	cin >> height_b;



	Rect rectA = Rect(x_a, y_a, xAnchor_a, yAnchor_a, width_a, height_a);
	Rect rectB = Rect(x_b, y_b, xAnchor_b, yAnchor_b, width_b, height_b);

	cout << "===========================" << endl;
	cout << "RESULT" << endl;
	cout << "===========================" << endl;
	if (rectA.didIntersectRect(rectB))
	{
		cout << "Rect A and Rect B intersected." << endl;
	}
	else
	{
		cout << "Rect A and Rect B did not intersect.";
	}
}


void main()
{
	
	//RECTANGLE
	rectResult();

	//CIRCLE
	circleResult();


	system("pause");

}